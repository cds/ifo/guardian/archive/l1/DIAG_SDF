from SYS_DIAG import *

import cdslib


##################################################

FEMODELS = list(cdslib.get_all_models())

EXCLUDE_LIST = [
#    'iopsusauxb123',
    'brsex',
    'brsey',
]

@SYSDIAG.register_test
def DIFFS():
    """SDF diffs"""
    for cm in FEMODELS:
        if cm.name in EXCLUDE_LIST:
            continue
        n = cm.SDF_DIFF_CNT
        if n > 0:
            yield '{sys}: {n}'.format(sys=cm.name, n=n)

##################################################

if __name__ == '__main__':

    from ezca import Ezca
    Ezca().export()

    for cm in FEMODELS:
        if cm.name in EXCLUDE_LIST:
            continue
        print(cm.name, cm.fec)
    # for m in DIFFS():
    #     try:
    #         print(m)
    #     except ezca.errors.EzcaConnectError as e:
    #         print(e)
